import { all, fork } from 'redux-saga/effects';
// import watchUserAuthentication from './watchers';
import { authSaga } from './authSaga';

// export default function* startForman() {
//     yield fork(watchUserAuthentication);
// }

function* rootSaga() {
    yield all([
        fork(authSaga),
    ])
}

export default rootSaga