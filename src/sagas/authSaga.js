import { takeLatest, put, call, all, fork } from 'redux-saga/effects';

import { userService } from '../services/userService';
import { userActions } from '../actions/userActions';

import * as types from '../actions';

function* getTokenSaga(payload) {
    try {
        const response = yield call(userService.getToken, payload.creds)
        localStorage.setItem('user', JSON.stringify(response));
        yield put({ type: types.TOKEN_USER_SUCCESS, response });
    } catch (error) {
        yield put({ type: types.TOKEN_USER_ERROR, error });
    }
}

function* logout() {
    yield call(userService.logout);
    yield put(userActions.userLoggedOut());
}

function* watchUserSaga() {
    yield takeLatest(types.LOGIN_USER, getTokenSaga)
}

function* watchLogoutSaga() {
    yield takeLatest(types.LOGOUT_USER, logout);
}

export function* authSaga() {
    yield all([
        watchUserSaga(),
        watchLogoutSaga()
    ]);
}