import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { userService } from '../services/userService';
import { useHistory } from "react-router-dom";
import { history } from './history';

import{ userActions } from '../actions/userActions';

import rootReducer from '../reducer/rootReducer';
import rootSaga from '../sagas/rootSaga';

const saga = createSagaMiddleware()
const store = createStore(rootReducer, applyMiddleware(saga))

const user = userService.checkToken();

if(user){
    userService.validToken(function(response){
        if(response.status === "ok" && user){
            store.dispatch(userActions.userLoggedIn(user));
        }
    })
}

saga.run(rootSaga);

export default store;