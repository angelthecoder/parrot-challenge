import { createBrowserHistory } from 'history';

/**
 * Tener un elemento global para obtener y hacer push el history
 */
const history = createBrowserHistory();
export default history;