export function authHeader() {
    // regresa el header de autorización con el token 
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.access) {
        return { 
            'Authorization': 'Bearer ' + user.access,
            'Content-Type': 'application/json' 
        };
    } else {
        return {};
    }
}