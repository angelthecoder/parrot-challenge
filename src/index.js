'use strict';
import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom'

import store from './helpers/store';
import history  from './helpers/history';

import App from './components/App';


ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Route component={App} />
        </Router>
    </Provider>
  , document.getElementById('root')
);