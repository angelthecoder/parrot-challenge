import config from 'config';
import { authHeader } from '../helpers/auth-header'; 

export const userService = {
    getToken,
    checkToken,
    validToken,
    logout
};

/**
 * 
 * @param {username, password} request
 */

function getToken (request) {
    const LOGIN_API_ENDPOINT = `${config.apiUrl}/api/auth/token`;

    const parameters = {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(request)
    };

    return fetch(LOGIN_API_ENDPOINT, parameters)
            .then(response => {
                return response.json();
            })
            .then(json => {
                return json;
            });
};

/**
 * Validar Token en LocalStorage
 */
function validToken(cb){
    const VALIDATE_API_ENDPOINT = `${config.apiUrl}/api/auth/token/test`;
    let status;

    const parameters = {
        method: 'GET',
        headers: authHeader()
    };

    fetch(VALIDATE_API_ENDPOINT, parameters)
        .then(res => {
            return res.json();
        })
        .then(json => {
            status = json;
            cb(status);
        });
}


/**
 * Cerrar sesión
 */
function logout () {
    localStorage.removeItem('user');
}

/**
 * Validar si el token existe en el Storage
 */
function checkToken () {
    return JSON.parse(localStorage.getItem('user'))
}