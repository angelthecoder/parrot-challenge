import { TOKEN_USER_SUCCESS, TOKEN_USER_ERROR, LOGGEDIN_USER, LOGOUT_USER } from '../actions';

const initialState = null;

export const userReducer = (state = initialState, action) => {

    console.log(action.type);
    
    switch(action.type) {
        case LOGGEDIN_USER:
            return action.user;
        case LOGOUT_USER:
            return initialState;
        case TOKEN_USER_SUCCESS:
            return action.response;
        case TOKEN_USER_ERROR:
            return action.response;
        default:
            return state;
    }
};