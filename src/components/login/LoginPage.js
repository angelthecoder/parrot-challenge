import React, { Component } from 'react';
import { connect } from 'react-redux';

import { userActions } from '../../actions/userActions';

class LoginPage extends Component {

    onHandleLogin (event) {
        event.preventDefault();
        // let email = event.target.email.value;
        // let password = event.target.password.value;
        let email = 'android-challenge@parrotsoftware.io';// <-- Solo Test
        let password = '8mngDhoPcB3ckV7X'; // <-- Solo Test

        const creds = {
          email, password
        };

        this.props.login(creds);
    }


    render() {
        return (
            <div>
                <h3>Login Page</h3>
                <form onSubmit={this.onHandleLogin.bind(this)}>
                    <div>
                        <label>Email</label>
                        <input type="email" name="email" />
                    </div>
                    <div>
                        <label>Password</label>
                        <input type="password" name="password" />
                    </div>
                    <div>
                        <button>Login</button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        login: (creds) => {
            dispatch(userActions.getTokenAction(creds))
        }
    }
}

export default connect(null, mapDispatchToProps)(LoginPage);