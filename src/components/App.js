import React from 'react';

import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { userService } from '../services/userService';

import LoginPage from './login/LoginPage';
import ProductsPage from './products/ProductsPage';

import Header from './shared/Header';


function App (){
    return (
        <div>
            <div>
                <Header />
            </div>
            <div>
                <Switch>
                    <Route path="/" component={LoginPage} />
                    <Route path="/products" component={ProductPage} />
                </Switch>
            </div>
        </div>
    );
}

export default App;