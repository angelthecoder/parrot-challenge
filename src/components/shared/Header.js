import React, { Component } from 'react';
import { connect } from 'react-redux';

import { userActions } from '../../actions/userActions';

class Header extends Component{

    handleLogout(event){
        event.preventDefault();
        this.props.logout();
    };
    
    render(){
        let user = this.props.user
        
        return (
            <div>
                {user && <button onClick={this.handleLogout.bind(this)}>Logout</button>}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        logout: () => {
            dispatch(userActions.userLoggedOut())
        }
    }
}

export default connect(null, mapDispatchToProps)(Header);