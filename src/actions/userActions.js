import * as types from './index';

export const userActions = {
    getTokenAction,
    userLoggedIn,
    userLoggedOut,
};

/**
 * 
 * @param {username, password} creds 
 */
function getTokenAction(creds){
    return {
        type: types.LOGIN_USER,
        creds
    }
}

/**
 * 
 * @param {access, refresh} user 
 */
function userLoggedIn (user) {
    return {
        type: types.LOGGEDIN_USER,
        user
    }
}

function userLoggedOut () {
    return {
        type: types.LOGOUT_USER
    }
}