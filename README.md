## Motivación

El propósito de este boilerplate es resolver la prueba técnica de front end para [Parrot Software](parrotsoftware.io).

## Comenzar

Para comenzar, simplemente puedes descargar el boilerplate y descomprimirlo en tu directorio de trabajo o también puedes clonar si quieres contribuir.

### Prerequisitos

- Node.js v8 o superior

Puede verificar la versión de su node usando el comando:

```CLI
node --version
```

### Instalar

Instala las dependecias y comienza usando [yarn](https://yarnpkg.com):

```CLI
yarn install
yarn start
```

O vía [npm](https://www.npmjs.com/):

```CLI
npm install
npm start
```

### Pruebas

Puedes correr un test usando el comando:

```CLI
yarn test
```

Es posible generar la cobertura de código. Jest genera un archivo HTML con toda la información de tus pruebas. Para hacer esto, ejecuta el comando:

```CLI
yarn test:coverage
```

### Distribución

Puede generar un paquete de distribución optimizado. Para hacer esto, ejecuta el comando:

```CLI
yarn build
```

Es posible comprobar el tamaño y el contenido de tu archivo empaquetado. Para hacer esto, ejecuta el comando:

```CLI
yarn analyze
```

¡Y eso es todo!

## Construido con

- [webpack](https://webpack.js.org/)
- [React](https://babeljs.io/)
- [Babel](https://babeljs.io/)
- [Yarn](https://yarnpkg.com)
- [Jest](https://jestjs.io/)
- [Enzyme](https://airbnb.io/enzyme/docs/api/)
- [Redux](https://es.redux.js.org/)
- [Redux Saga](https://redux-saga.js.org/)
- [React Router](https://reactrouter.com/)
- [History](https://github.com/ReactTraining/history#readme)
- [Moment](https://momentjs.com/)
